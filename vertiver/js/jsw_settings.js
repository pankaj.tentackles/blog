(function ($, Drupal, undefined) {
  Drupal.behaviors.jsw = {
    attach: function (context, settings) {
      var type = settings.type.value;
      if (type == 'query') {
      	/*display pop up for query msg*/
	$('#trigMsg').modal('toggle');
      } 
      
      if (type == 'subscribe') {
	/*display pop up for newsletter subscribe*/
	$('#subMsg').modal('toggle');
      }
    }
  };

  //set time interval in blog page for 5 mins & open popup
  setInterval(function() {
    $('#myModal').modal('toggle');
  }, 300000);

})(jQuery, Drupal);