<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php function catch_first_image() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  $first_img = $matches [1] [0];

  if(empty($first_img)){ //Defines a default image
    $first_img = "/images/default.jpg";
  }
  return $first_img;
}

?>

<section>
        <a id="main-content"></a>
        <h1 class="page-header">Blogs</h1>
        <div class="region region-content">
            <section id="block-system-main" class="block block-system clearfix">
                <div class="container-fluid main_blog">
    			    <div class="container">
      			        <div class="row">
                            <div class="col-sm-8">
                           
     <!-- First Loop start  -->
<?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=1&offset=0'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count1 = 0; if ($count1 == "1") { break; } else { ?>
<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>


<img src="<?php echo $feat_image;  ?>"  />
<h4 class="titletopblog"><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>">
<?php the_title(); ?></a></h4>

<p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span> <a href="<?php echo get_comments_link( $post->ID ); ?>">
    comments
</a></span></p>

                            
                                
            	                <h5 class="h5top"><?php echo wp_trim_words( get_the_content(), 40, '...' ); ?></h5>  

<?php $count1++; } ?>
<?php endforeach; ?>
  <!-- First Loop end  -->   
         	  
        	                    <hr>
                                <div class="col-sm-6">
                                <!-- second Loop start  -->
                                    <?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=1&offset=1'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count2 = 0; if ($count2 == "1") { break; } else { ?>

<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>


<img src="<?php echo $feat_image;   ?>"  style="width:377px;height: 231px;" />
<h4 class="h4top"><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>" style="color:#000000;text-decoration:none;">
<?php the_title(); ?></a></h4>


<p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span><a href="<?php echo get_comments_link( $post->ID ); ?>">
    comments
</a></span></p>

<?php $count1++; } ?>
<?php endforeach; ?>

<!-- 2 Loop end  -->
                                </div>
                                <div class="col-sm-6">
                                <!--3 loop -->
                                   <?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=1&offset=2'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count3 = 0; if ($count3 == "1") { break; } else { ?>

<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>


<img src="<?php echo $feat_image;  ?>" style="width:377px;height: 231px;" />
<h4 class="h4top"><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>" style="color:#000000;text-decoration:none;">
<?php the_title(); ?></a></h4>


<p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span><a href="<?php echo get_comments_link( $post->ID ); ?>">
    comments
</a></span></p>

<?php $count1++; } ?>
<?php endforeach; ?>
               <!-- 3rd loop end -->                 </div>
                            </div>
                        <style>
                            .subscribe-us #edit-mail
                            {
                                font-style: initial !important;
                                color: #000000 !important;
                                width: 100% !important;
                                float: initial !important;
                                width: 100% !important;
                            }
                            .subscribe-us #edit-submit
                            {
                                margin: 0 !important;
                                height: 40px !important;
                                background: none !important;
								border: 1px solid #fff;
                            }
							.subscribe_btn:hover { background: #4fb748; color:#fff !important; }
							button#edit-submit:hover { background: #4fb748 !important; color:#fff !important;}
                        </style>
                        <div class="col-sm-4 right_blog right_blognew">
                            <div class="subscribe-us">
                                <h3 style="font-weight: bold; text-transform: capitalize; text-align: center;">Connect With Us</h3>
                                <p style="text-align: center;">Passionate about design thinking, sustainability, craft, massive change and related ideas?</p>
                                    <form action="#" method="post" id="simple-subscription-form" accept-charset="UTF-8">
                                        <div>
                                            <div class="form-type-textfield form-item-mail form-item form-group">
                                                <input placeholder="Enter email address" class="form-control form-text required" type="text" id="edit-mail" name="mail" value="" size="20" maxlength="255" />
                                            </div>
                                            <button class="btn btn-default form-submit" id="edit-submit" name="op" value="Subscribe" type="submit">Join us!</button>
                                            <input type="hidden" name="form_build_id" value="form--sv6zjmY-oh9pcVZJZ20AJCj_uBlq84Iql9DlQ2_Goc" />
                                            <input type="hidden" name="form_id" value="simple_subscription_form" />
                                        </div>
                                    </form>
                                </div>
                                <!-- title loop -->
                                <?php query_posts('showposts=3'); ?>
<?php $posts = get_posts('numberposts=3&offset=0'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count4 = 0; if ($count4 == "3") { break; } else { ?>
<hr>
<h4 class="h4top" style="margin-top:0px;margin-bottom:0px;"><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>" style="color:#000000;text-decoration:none;">
<?php the_title(); ?></a></h4>
<p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span></p>

<?php $count1++; } ?>
<?php endforeach; ?>
                                
              <!-- title loop end -->                  
                                <hr>
                                <h4 style="background-color:#f7f7f7;border-top:4px solid #F89938">Featured</h4>
                                <div class="view view-popular-articles view-id-popular_articles view-display-id-block view-dom-id-cc9cae092084b9e436c463351ddc9215">
                                    <div class="view-content">
                                       <!-- all the hit loop -->
                          <?php query_posts('showposts=4'); ?>
<?php $posts = get_posts('numberposts=4&offset=0'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count11 = 0; if ($count11 == "4") { break; } else { ?>

<div class="popular-articles">
                                            <div class="views-field views-field-field-image">
                                                <div class="field-content">

<img src="<?php echo catch_first_image() ?>" />
 </div>
                                            </div>  
                                            <div>
<p><a href="<?php the_permalink() ?>" rel="bookmark"  title="Permanent Link to <?php the_title_attribute(); ?>" style="color:#000000;text-decoration:none;">
<?php the_title(); ?></a></p>
</div>
                                        </div>


<?php $count1++; } ?>
<?php endforeach; ?>
<!-- all the hit loop end-->
                                    </div>
                                </div>
                            </div>
                        </div>
  				    </div>
  			  </div>
              <div class="container-fluid updates">
                  <div class="container text-center">
      			      <h1 style="color:#4fb748; text-transform: inherit;" class="col-sm-12">We at India Forest Portal are building a community of people dedicated to conservation. Join us!</h1>
      			      <button style="color: #4fb748; border: 1px solid #4fb748;"class="btn subscribe_btn" onclick="location.href='http://indiaforestportal.com/pledge-today.html';">Pledge Today</button>
    			  </div>
  			  </div>
              <div class="container-fluid">
    			  <div class="container">
      			      <div class="row">
                          <div class="col-sm-4 blog_border" >
                          <!-- 4 loop -->
                          <?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=1&offset=3'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count5 = 0; if ($count5 == "1") { break; } else { ?>
<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

<img src="<?php echo $feat_image;  ?>" style="width:377px; height:231px" />
<div class="blog_content">
<h4 class="h4top"><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>" style="color:#000000;text-decoration:none;">
<?php the_title(); ?></a></h4>


<p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span><a href="<?php echo get_comments_link( $post->ID ); ?>">
    comments
</a></span></p>
</div>
<?php $count1++; } ?>
<?php endforeach; ?>
<!-- 4 loop end-->
					      </div>
                          <div class="col-sm-4 blog_border" style="padding-left:7.5px;padding-right:7.5px">
                              <!-- 5 loop -->
                          <?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=1&offset=4'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count6 = 0; if ($count6 == "1") { break; } else { ?>
<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
<img src="<?php echo $feat_image;  ?>" style="width:377px; height:231px" />
<div class="blog_content">
<h4><a href="<?php the_permalink() ?>" rel="bookmark"  title="Permanent Link to <?php the_title_attribute(); ?>" style="color:#000000;text-decoration:none;">
<?php the_title(); ?></a></h4>


<p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span><a href="<?php echo get_comments_link( $post->ID ); ?>">
    comments
</a></span></p>
</div>
<?php $count1++; } ?>
<?php endforeach; ?>
<!-- 5 loop end-->
					      </div>
                          <div class="col-sm-4 blog_border" >
                              <!-- 6 loop -->
                          <?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=1&offset=5'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count7 = 0; if ($count7 == "1") { break; } else { ?>
<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
<img src="<?php echo $feat_image;  ?>" style="width:377px; height:231px" />
<div class="blog_content">
<h4><a href="<?php the_permalink() ?>" rel="bookmark"  title="Permanent Link to <?php the_title_attribute(); ?>" style="color:#000000;text-decoration:none;">
<?php the_title(); ?></a></h4>


<p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span><a href="<?php echo get_comments_link( $post->ID ); ?>">
    comments
</a></span></p>
</div>
<?php $count1++; } ?>
<?php endforeach; ?>
<!-- 6 loop end-->
					      </div>
                          <div class="col-sm-4 blog_border" >
                              <!-- 7 loop -->
                          <?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=1&offset=6'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count8 = 0; if ($count8 == "1") { break; } else { ?>
<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
<img src="<?php echo $feat_image;  ?>" style="width:377px; height:231px" />
<div class="blog_content">
<h4><a href="<?php the_permalink() ?>" rel="bookmark"  title="Permanent Link to <?php the_title_attribute(); ?>" style="color:#000000;text-decoration:none;">
<?php the_title(); ?></a></h4>


<p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span><a href="<?php echo get_comments_link( $post->ID ); ?>">
    comments
</a></span></p>
</div>
<?php $count1++; } ?>
<?php endforeach; ?>
<!-- 7loop end-->
					      </div>
                          <div class="col-sm-4 blog_border" style="padding-left:7.5px;padding-right:7.5px">
                              <!-- 8loop -->
                          <?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=1&offset=7'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count9 = 0; if ($count9 == "1") { break; } else { ?>
<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
<img src="<?php echo $feat_image;  ?>" style="width:377px; height:231px" />
<div class="blog_content">
<h4><a href="<?php the_permalink() ?>" rel="bookmark"  title="Permanent Link to <?php the_title_attribute(); ?>" style="color:#000000;text-decoration:none;">
<?php the_title(); ?></a></h4>


<p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span><a href="<?php echo get_comments_link( $post->ID ); ?>">
    comments
</a></span></p>
</div>
<?php $count1++; } ?>
<?php endforeach; ?>
<!-- 8loop end-->
					      </div>
                          <div class="col-sm-4 blog_border" >
                              <!-- 9loop -->
                          <?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=1&offset=8'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count10 = 0; if ($count10 == "1") { break; } else { ?>
<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
<img src="<?php echo $feat_image;  ?>" style="width:377px; height:231px" />
<div class="blog_content">
<h4><a href="<?php the_permalink() ?>" rel="bookmark"  title="Permanent Link to <?php the_title_attribute(); ?>" style="color:#000000;text-decoration:none;">
<?php the_title(); ?></a></h4>


<p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span><a href="<?php echo get_comments_link( $post->ID ); ?>">
    comments
</a></span></p>
</div>
<?php $count1++; } ?>
<?php endforeach; ?>
<!-- 9loop end-->
					      </div>
                          <!-- 15loop -->
                          <?php query_posts('showposts=2'); ?>
<?php $posts = get_posts('numberposts=2&offset=19'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count17= 0; if ($count17 == "2") { break; } else { ?>

<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>


					<div class="col-sm-4 blog_border"> <img src="<?php echo $feat_image;  ?>" style="width:377px; height:231px" />
                              <div class="blog_content">
						           <h4><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>">
<?php the_title(); ?></a></h4>
                                 <p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span> <a href="<?php echo get_comments_link( $post->ID ); ?>">
    comments
</a></span></p>
					          </div>
					      </div>	 
						   
				
  				
                           <?php $count1++; } ?>
<?php endforeach; ?>
<!-- 15loop end-->


                          <div class="col-sm-4 blog_border">
                              <div class="get_book getbooknew" style="padding-bottom:19px;">
                                  <h3 class="ebookh3">Join the REVERT FLEX ACCORD
</h3>
                                  <p class="ebookp" style=""><br>The Revert Flex Accord is the first step we can take to minimize the environmental footprint of flex.</p>
                                  <a class="ebooka btn red-btn"  href="http://vertiver.com/revert/flex_accord.html"  style="margin-top:20px;background:#E88A0E;color:#000000;">SIGN FLEX ACCORD </a>
                              </div>
					      </div>
                      </div>
  			      </div>


                  <!-- 16loop -->
                          <?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=1&offset=21'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count18= 0; if ($count18 == "1") { break; } else { ?>

<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
				  
                  <div class="container seo_desc">
      			      <div class="row">
                          <div class="col-sm-8">
          		              <a class="titlehreftop" href="#">
                              <img src="<?php echo $feat_image;  ?>"  /></a>
        		          </div>
                          <div class="col-sm-4 blog_master">
          		              <div class="blogs">
            		               <h4><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>">
<?php the_title(); ?></a></h4>
            		             <p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span> <a href="<?php echo get_comments_link( $post->ID ); ?>">
    comments
</a></span></p>
                                 
                              </div>
                          </div>
                      </div>
    		      </div>
				  
  				  <?php $count1++; } ?>
<?php endforeach; ?>
<!-- 16loop end-->
                  <div class="container">
      			      <div class="row">

                           <!-- 12loop -->
                          <?php query_posts('showposts=3'); ?>
<?php $posts = get_posts('numberposts=3&offset=14'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count13= 0; if ($count13 == "3") { break; } else { ?>

<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>


					<div class="col-sm-4 blog_border"> <img src="<?php echo $feat_image;  ?>" style="width:377px; height:231px"  />
                              <div class="blog_content">
						           <h4><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>">
<?php the_title(); ?></a></h4>
                                 <p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span> <a href="<?php echo get_comments_link( $post->ID ); ?>">
    comments 
</a></span></p>
					          </div>
					      </div>	 
						   
				
  				  <?php $count1++; } ?>
<?php endforeach; ?>
<!-- 12loop end-->
                         

                           <!-- 10loop -->
                          <?php query_posts('showposts=3'); ?>
<?php $posts = get_posts('numberposts=3&offset=9'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count11 = 0; if ($count11 == "3") { break; } else { ?>

<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>


					<div class="col-sm-4 blog_border"> <img src="<?php echo $feat_image;  ?>" style="width:377px; height:231px" />
                              <div class="blog_content">
						           <h4><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>">
<?php the_title(); ?></a></h4>
                                 <p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span> <a href="<?php echo get_comments_link( $post->ID ); ?>">
    comments
</a></span></p>
					          </div>
					      </div>	 
						   
				
  				  <?php $count1++; } ?>
<?php endforeach; ?>
<!-- 10loop end-->
		

                      </div>
      			  </div>
                   <!-- 17loop big image -->
                          <?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=1&offset=22'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count19= 0; if ($count19 == "1") { break; } else { ?>

<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
				  
                  <div class="container seo_desc">
      			      <div class="row">
                          <div class="col-sm-8">
          		              <a class="titlehreftop" href="#">
                              <img src="<?php echo $feat_image;  ?>" height="600px" width="900px"  /></a>
        		          </div>
                          <div class="col-sm-4 blog_master">
          		              <div class="blogs">
            		               <h4><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>">
<?php the_title(); ?></a></h4>
            		             <p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span> <a href="<?php echo get_comments_link( $post->ID ); ?>">
    comments
</a></span></p>
                                 
                              </div>
                          </div>
                      </div>
    		      </div>
				  
  				  <?php $count1++; } ?>
<?php endforeach; ?>
<!-- 117loop end-->
                  <div class="container">
      			      <div class="row">


                          <!-- 11loop -->
                          <?php query_posts('showposts=3'); ?>
<?php $posts = get_posts('numberposts=3&offset=12'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count12 = 0; if ($count12 == "3") { break; } else { ?>

<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>


					<div class="col-sm-4 blog_border"> <img src="<?php echo $feat_image;  ?>" style="width:377px; height:231px"  />
                              <div class="blog_content">
						           <h4><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>">
<?php the_title(); ?></a></h4>
                                 <p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span> <a href="<?php echo get_comments_link( $post->ID ); ?>">
    comments
</a></span></p>
					          </div>
					      </div>	 
						   
				
  				  <?php $count1++; } ?>
<?php endforeach; ?>
<!-- 11loop end-->
                         
                          
                          <!-- 13loop -->
                          <?php query_posts('showposts=3'); ?>
<?php $posts = get_posts('numberposts=3&offset=16'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count14= 0; if ($count14 == "3") { break; } else { ?>

<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>


					<div class="col-sm-4 blog_border"> <img src="<?php echo $feat_image;  ?>" style="width:377px; height:231px" />
                              <div class="blog_content">
						           <h4><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>">
<?php the_title(); ?></a></h4>
                                 <p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span> <a href="<?php echo get_comments_link( $post->ID ); ?>">
    comments 
</a></span></p>
					          </div>
					      </div>	 
						   
				
  				  <?php $count1++; } ?>
<?php endforeach; ?>
<!-- 13loop end-->
                      </div>
      			  </div>
                  <!-- 18loop big image -->
                          <?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=1&offset=5'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count20= 0; if ($count20 == "1") { break; } else { ?>

<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
				  
                  <div class="container seo_desc">
      			      <div class="row">
                          <div class="col-sm-8">
          		              <a class="titlehreftop" href="#">
                              <img src="<?php echo $feat_image;  ?>" width="900px" height="600px"  /></a>
        		          </div>
                          <div class="col-sm-4 blog_master">
          		              <div class="blogs">
            		               <h4><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>">
<?php the_title(); ?></a></h4>
            		             <p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span> <a href="<?php echo get_comments_link( $post->ID ); ?>">
    comments
</a></span></p>
                                 
                              </div>
                          </div>
                      </div>
    		      </div>
				  
  				  <?php $count1++; } ?>
<?php endforeach; ?>
<!-- 18loop end-->
                  <div class="container">
      			      <div class="row">
                          <!-- 14loop -->
                          <?php query_posts('showposts=3'); ?>
<?php $posts = get_posts('numberposts=3&offset=19'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count16= 0; if ($count16 == "3") { break; } else { ?>

<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>


					<div class="col-sm-4 blog_border"> <img src="<?php echo $feat_image;  ?>" style="width:377px; height:231px" />
                              <div class="blog_content">
						           <h4><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>">
<?php the_title(); ?></a></h4>
                                 <p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span> <a href="<?php echo get_comments_link( $post->ID ); ?>">
    comments 
</a></span></p>
					          </div>
					      </div>	 
						   
				
  				  <?php $count1++; } ?>
<?php endforeach; ?>
<!-- 14loop end-->
                         
      			  </div>
                 <!-- 19loop big image -->
                          <?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=1&offset=1'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count21= 0; if ($count21 == "1") { break; } else { ?>

<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
				  
                  <div class="container seo_desc">
      			      <div class="row">
                          <div class="col-sm-8">
          		              <a class="titlehreftop" href="#">
                              <img src="<?php echo $feat_image;  ?>"  /></a>
        		          </div>
                          <div class="col-sm-4 blog_master">
          		              <div class="blogs">
            		               <h4><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>">
<?php the_title(); ?></a></h4>
            		             <p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span> <a href="<?php echo get_comments_link( $post->ID ); ?>">
    comments
</a></span></p>
                                 
                              </div>
                          </div>
                      </div>
    		      </div>
				  
  				  <?php $count1++; } ?>
<?php endforeach; ?>
<!-- 19loop end-->
                <div class="container">
    			    <div class="row">
      			        <div class="daily_updates" style="border: none; background: #f7f7f7;">
        			        <h1 style="color: #4fb748; font-size: 32px; text-transform: inherit;" class="col-sm-12">Vertiver is a creative agency working on<br> climate action and sustainable development.</h1>
        			        <h1 style="color: #666; font-size: 32px; text-transform: inherit;">Partner with us to do good for the planet.</h1>
                              <br>
                               <div class="col-sm-12">
                                <form action="#" method="post" id="simple-subscription-form--2" accept-charset="UTF-8">
                                    <div>
                                        <div class="form-type-textfield form-item-mail form-item form-group">
                                            <input style="background: none; padding: 10px; border: solid 1px #ccc;" placeholder="Enter email address" class="form-control form-text required" type="text" id="edit-mail--2" name="mail" value="" size="20" maxlength="255" />
                                        </div>
                                        <button class="btn btn-default form-submit" id="edit-submit--2" name="op" value="Subscribe" type="submit">Sign Up</button>
                                        <input type="hidden" name="form_build_id" value="form-14LX8fhxGH8pw4nn9Mz02CJjAmDEO5u3VSaZid5vNgA" />
                                        <input type="hidden" name="form_id" value="simple_subscription_form" />
                                    </div>
                                </form>
                            </div>
      			        </div>
    			    </div>
  			    </div><!-- Modal -->
                <div id="myBlogModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content subscribe_body">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <h2>If you are enjoying what you are<br/> reading, why not subscribe to our blog!</h2>
                                <p>We promise to deliver you one hell of an intersting emailer every week telling you about what is happening in the content marketing industry, about content strategies of different companies, and content best practices from the around world!</p>
                                <p>This one is not your average newsletter!</p>
                                <p><form action="#" method="post" id="simple-subscription-form--3" accept-charset="UTF-8">
                                    <div>
                                        <div class="form-type-textfield form-item-mail form-item form-group">
                                            <input class="form-control form-text required" type="text" id="edit-mail--3" name="mail" value="" size="20" maxlength="255" />
                                        </div>
                                        <button class="btn btn-default form-submit" id="edit-submit--3" name="op" value="Subscribe" type="submit">Subscribe</button>
                                        <input type="hidden" name="form_build_id" value="form-Tgbe0A07BvIadozoyCcIgzqGE3DDMWjKw_Xdedarbvg" />
                                        <input type="hidden" name="form_id" value="simple_subscription_form" />
                                    </div>
                                </form></p>
                            </div>
                            <!--div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div-->
                        </div>
                    </div>
                </div>
            </section> <!-- /.block -->
        </div>
    </section>
    <!-- Subscribe Modal -->
    <div id="subMsg" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content subscribe_body">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        &times;</button>
                </div>
                <div class="modal-body">
                    <h2 class="modal-title">
                        Thanks, you are signed<br>
                        up for our blog updates!
                    </h2>
                    <p>
                        We would love your help in spreading the word about the Justwords blog!
                    </p>
                    <button class="btn btn-subsc">
                        Click here to go to our latest article.</button>
                </div>
                <!--div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div-->
            </div>
        </div>
    </div>
    <!-- subscribe modal ends here -->
    
    

<?php get_footer();
