<?php

/**

 * The template for displaying the footer

 *

 * Contains the closing of the #content div and all content after.

 *

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

 *

 * @package WordPress

 * @subpackage Twenty_Seventeen

 * @since 1.0

 * @version 1.0

 */



?>

<footer>

      <h1 class="be-trim">Be a trimtab </h1>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <ul class="footer-contact2">
                            <!--<li>Contact</li>-->
                            <p>Vertiver <br>225-B, Lado Sarai,<br> Indraprastha, Complex<br> New Delhi- 110030
                                <br> Phone: 011-46060916 <br>info@vertiver.com</p></ul>
                    </div>
                    <div class="col-md-6">
                        <ul class="socialicon">
                            <li><a href="#"><i class="fa  fa-vimeo"></i></a></li>
                            <li><a href="https://www.facebook.com/vertiver" target="_blank"><i class="fa  fa-facebook"></i></a>
                            </li>
                            <li><a href="#" target="_blank"><i class="fa fa-pinterest-p"></i></a></li>
                            <li><a target="_blank" href="https://www.youtube.com/user/AgencyVertiver"><i
                                    class="fa  fa-youtube-play"></i></a></li>
                            <li><a href="https://www.linkedin.com/company/vertiver/" target="_blank"><i
                                    class="fa fa-linkedin"></i></a></li>
                            <li><a href="https://twitter.com/search?q=vertiver&src=sprv" target="_blank"><i
                                    class="fa  fa-twitter"></i></a></li>
                        </ul>

                        <form method="POST" action="#" id="formcontactus">
                            <div class="form-group animated fadeIn visible" data-animation="fadeIn" data-delay="600" style="padding-left:unset">
                                <input name="email" type="email" class="form-control input-lg requiretop" placeholder="Email Id">
                                <span class="leftarrow2"><i class="fa fa-arrow-right"></i></span>
                            </div>

                        </form>
                    </div>

                    <div class="col-md-12">
                        <ul> Go to <a class="nav-cosmos" href="world-cosmos.html" target="_blank"><b><span
                                style="color: #4eb648">Vertiver</span><span style="color: #ff9200"> Lexicon</span></b></a>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row copyrights"><p>Copyright © Vertiver, 2018. All rights reserved</p></div>
    </footer>

    <script src="sites/all/modules/addthis/addthisd3d0.js?ol9ljc"></script>

    <script src="sites/all/themes/bootstrap/js/bootstrapd3d0.js?ol9ljc"></script>

<?php wp_footer(); ?>



</body>

</html>

