<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html lang="en" dir="ltr" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:content="http://purl.org/rss/1.0/modules/content/">
<head>
    <title>Blog | vertiver</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php echo bloginfo('url'); ?>/vertiver/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo bloginfo('url'); ?>/vertiver/css/overrides.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo bloginfo('url'); ?>/vertiver/css/siderbarsocial.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo bloginfo('url'); ?>/vertiver/css/simple_subscription.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo bloginfo('url'); ?>/vertiver/css/styles.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo bloginfo('url'); ?>/vertiver/css/views.css" rel="stylesheet" type="text/css" />
    <!-- HTML5 element support for IE6-8 -->
    <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
    <script type="text/javascript" src="<?php echo bloginfo('url'); ?>/vertiver/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo bloginfo('url'); ?>/vertiver/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo bloginfo('url'); ?>/vertiver/js/simple_subscription.js"></script>
    <script type="text/javascript" src="<?php echo bloginfo('url'); ?>/vertiver/js/bootstrap.min.js"></script>
   <!-- <script type="text/javascript" src="<?php echo bloginfo('url'); ?>/vertiver/js/sidebarsociallogineplugin.js"></script>
    <script type="text/javascript" src="<?php echo bloginfo('url'); ?>/vertiver/js/sidebarsociallogin.js?ol9ljc"></script>
    <script type="text/javascript">        jQuery.extend(Drupal.settings, { "basePath": "\/", "pathPrefix": "", "ajaxPageState": { "theme": "justwords", "theme_token": "HaBS6_TFslTPqWvz0EeVQN3CwESndDyIFvHzy0mWiz8", "js": { "sites\/all\/modules\/addthis\/addthis.js": 1, "sites\/all\/themes\/bootstrap\/js\/bootstrap.js": 1, "sites\/all\/modules\/jquery_update\/replace\/jquery\/1.11\/jquery.min.js": 1, "misc\/jquery.once.js": 1, "misc\/drupal.js": 1, "\/\/netdna.bootstrapcdn.com\/bootstrap\/3.0.2\/js\/bootstrap.min.js": 1, "sites\/all\/modules\/simple_subscription\/simple_subscription.js": 1, "sites\/all\/themes\/justwords\/bootstrap\/js\/bootstrap.min.js": 1, "sites\/all\/themes\/justwords\/js\/sidebarsociallogineplugin.js": 1, "sites\/all\/themes\/justwords\/js\/sidebarsociallogin.js": 1 }, "css": { "modules\/system\/system.base.css": 1, "modules\/field\/theme\/field.css": 1, "sites\/all\/modules\/logintoboggan\/logintoboggan.css": 1, "sites\/all\/modules\/views\/css\/views.css": 1, "sites\/all\/modules\/ckeditor\/css\/ckeditor.css": 1, "sites\/all\/modules\/ctools\/css\/ctools.css": 1, "sites\/all\/modules\/simple_subscription\/simple_subscription.css": 1, "\/\/netdna.bootstrapcdn.com\/bootstrap\/3.0.2\/css\/bootstrap.min.css": 1, "sites\/all\/themes\/bootstrap\/css\/overrides.css": 1, "sites\/all\/themes\/justwords\/bootstrap\/css\/bootstrap.css": 1, "sites\/all\/themes\/justwords\/css\/styles.css": 1, "sites\/all\/themes\/justwords\/css\/siderbarsocial.css": 1} }, "simple_subscription": { "input_content": "Enter your email here" }, "urlIsAjaxTrusted": { "\/blog": true }, "bootstrap": { "anchorsFix": 1, "anchorsSmoothScrolling": 1, "popoverEnabled": 0, "popoverOptions": { "animation": 1, "html": 0, "placement": "right", "selector": "", "trigger": "click hover focus manual", "title": "", "content": "", "delay": 0, "container": "body" }, "tooltipEnabled": 1, "tooltipOptions": { "animation": 1, "html": 0, "placement": "auto left", "selector": "", "trigger": "hover focus", "delay": 0, "container": "body"}} });</script> -->
</head>
<body class="html not-front not-logged-in no-sidebars page-blog">
    <!--<div id="skip-link">
        <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
    </div>-->
    <!--<script src="sites/all/modules/custome/jsw/js/jsw.js"></script>
    <link rel="stylesheet" href="../maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">-->
    <div class="conainer-fluid">
        <header class="navbar" id="topnav">
            <div class="">
                <div class="navbar-header">
                    <div class="navbar-brand">
                        <a href="index.html">
                            <img src="images/logo.png" alt="vertiver logo">
                        </a>
                    </div>
                </div>

            </div>
        </header>
        <aside id="menu-right">
            <div id="particles-js"></div>
            <div id="menu-toggle">
                <span class="lines"></span>
                <span class="lines"></span>
                <span class="lines"></span>
                <span style="position: absolute; top: -6px; left: -31px; font-size: 32px; background: none;"></span>
            </div>

            <nav>
                <ul id="main-menu" class="nav">
                    <li><a href="index.html">Home</a></li>
                    <li><a href="services.html">Services</a></li>
                    <li><a href="work.html">Work</a></li>
                    <li><a href="initiatives.html">Initiatives</a></li>
                    <li><a href="about.html">About</a></li>
                    <li><a href="http://vertiver.com/blog/">Blog</a></li>
                    <li><a href="contact.html">Contact</a></li>
                    <!--<li><a href="connect.html">Connect</a></li>-->
                </ul>

                <ul class="word-cosmos">
                    <a class="nav-cosmos" href="world-cosmos.html" target="_blank"><b><span
                            style="color: #4eb648">Vertiver</span><span style="color: #ff9200"> Lexicon</span></b></a>

                </ul>
                <ul id="socials" class="nav">
                    <li><a href="https://www.facebook.com/vertiver" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/search?q=vertiver&src=sprv" target="_blank"><i
                            class="fa fa-twitter"></i></a></li>

                   <li><a href="https://www.pinterest.com/vertiver/" target="_blank"><i class="fa fa-pinterest"></i></a> </li>
        
                   <li><a href="https://www.youtube.com/user/AgencyVertiver" target="_blank"><i class="fa fa-youtube"></i></a>
                    </li>

                    <li><a href="https://vimeo.com/vertiver" target="_blank"><i class="fa fa-vimeo"></i></a> </li>
                    <!-- <li><a href="https://plus.google.com/100499288410097177082" target="_blank"><i
                            class="fa fa-google-plus"></i></a></li> -->
                   
                    <li><a href="https://www.linkedin.com/company/vertiver/" target="_blank"><i class="fa fa-linkedin"></i></a>
                    </li>

                    
                    <!--<li><a href="#"><i class="fa fa-pinterest"></i></a></li>-->
                </ul>
            </nav>
        </aside>
    </div>
    <header role="banner" id="page-header"></header>
    <!-- /#page-header -->

<?php wp_head(); ?>
</head>

