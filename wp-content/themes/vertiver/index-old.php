<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php function catch_first_image() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  $first_img = $matches [1] [0];

  if(empty($first_img)){ //Defines a default image
    $first_img = "/images/default.jpg";
  }
  return $first_img;
}

?>

<section>
        <a id="main-content"></a>
        <h1 class="page-header">Blogs</h1>
        <div class="region region-content">
            <section id="block-system-main" class="block block-system clearfix">
                <div class="container-fluid main_blog">
    			    <div class="container">
      			        <div class="row">
                            <div class="col-sm-8">
                           
     <!-- First Loop start  -->
<?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=1&offset=0'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count1 = 0; if ($count1 == "1") { break; } else { ?>
<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>


<img src="<?php echo $feat_image;  ?>"  />
<h4 class="titletopblog"><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>">
<?php the_title(); ?></a></h4>

<p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span> <a href="<?php echo get_comments_link( $post->ID ); ?>">
    Comments to this post
</a></span></p>

<?php $count1++; } ?>
<?php endforeach; ?>
  <!-- First Loop end  -->                               
                                
            	                <h5 class="h5top">Have you ever felt stuck? Have you ever felt like you cannot, for the life of you, come up with an idea that works? Have you ever felt so ...</h5>           	  
        	                    <hr>
                                <div class="col-sm-6">
                                <!-- second Loop start  -->
                                    <?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=1&offset=1'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count2 = 0; if ($count2 == "1") { break; } else { ?>

<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>


<img src="<?php echo $feat_image;   ?>"  style="width:auto;height: 300px;" />
<h4 class="h4top"><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>" style="color:#000000;text-decoration:none;">
<?php the_title(); ?></a></h4>


<p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span><a href="<?php echo get_comments_link( $post->ID ); ?>">
    Comments to this post
</a></span></p>

<?php $count1++; } ?>
<?php endforeach; ?>

<!-- 2 Loop end  -->
                                </div>
                                <div class="col-sm-6">
                                <!--3 loop -->
                                   <?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=1&offset=2'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count3 = 0; if ($count3 == "1") { break; } else { ?>

<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>


<img src="<?php echo $feat_image;  ?>"  />
<h4 class="h4top"><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>" style="color:#000000;text-decoration:none;">
<?php the_title(); ?></a></h4>


<p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span><a href="<?php echo get_comments_link( $post->ID ); ?>">
    Comments to this post
</a></span></p>

<?php $count1++; } ?>
<?php endforeach; ?>
               <!-- 3rd loop end -->                 </div>
                            </div>
                        <style>
                            .subscribe-us #edit-mail
                            {
                                font-style: initial !important;
                                color: #000000 !important;
                                width: 100% !important;
                                float: initial !important;
                                width: 100% !important;
                            }
                            .subscribe-us #edit-submit
                            {
                                margin: 0 !important;
                                height: 40px !important;
                                background: #000 !important;
                            }
                        </style>
                        <div class="col-sm-4 right_blog right_blognew">
                            <div class="subscribe-us">
                                <h3 style="font-weight: bold;">SUBSCRIBE TO US</h3>
                                <p>Subscribe to our email newsletter for useful tips and resources.</p>
                                    <form action="#" method="post" id="simple-subscription-form" accept-charset="UTF-8">
                                        <div>
                                            <div class="form-type-textfield form-item-mail form-item form-group">
                                                <input class="form-control form-text required" type="text" id="edit-mail" name="mail" value="" size="20" maxlength="255" />
                                            </div>
                                            <button class="btn btn-default form-submit" id="edit-submit" name="op" value="Subscribe" type="submit">Subscribe</button>
                                            <input type="hidden" name="form_build_id" value="form--sv6zjmY-oh9pcVZJZ20AJCj_uBlq84Iql9DlQ2_Goc" />
                                            <input type="hidden" name="form_id" value="simple_subscription_form" />
                                        </div>
                                    </form>
                                </div>
                                <!-- title loop -->
                                <?php query_posts('showposts=3'); ?>
<?php $posts = get_posts('numberposts=3&offset=0'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count4 = 0; if ($count4 == "3") { break; } else { ?>
<hr>
<h4 class="h4top" style="margin-top:0px;margin-bottom:0px;"><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>" style="color:#000000;text-decoration:none;">
<?php the_title(); ?></a></h4>
<p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span></p>

<?php $count1++; } ?>
<?php endforeach; ?>
                                
              <!-- title loop end -->                  
                                <hr>
                                <h4 style="background-color:#f7f7f7;border-top:4px solid #e62732;">All Time Hits</h4>
                                <div class="view view-popular-articles view-id-popular_articles view-display-id-block view-dom-id-cc9cae092084b9e436c463351ddc9215">
                                    <div class="view-content">
                                       <!-- all the hit loop -->
                          <?php query_posts('showposts=4'); ?>
<?php $posts = get_posts('numberposts=4&offset=0'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count11 = 0; if ($count11 == "4") { break; } else { ?>

<div class="popular-articles">
                                            <div class="views-field views-field-field-image">
                                                <div class="field-content">

<img src="<?php echo catch_first_image() ?>"  />
 </div>
                                            </div>  
                                            <div>
<p><a href="<?php the_permalink() ?>" rel="bookmark"  title="Permanent Link to <?php the_title_attribute(); ?>" style="color:#000000;text-decoration:none;">
<?php the_title(); ?></a></p>
</div>
                                        </div>


<?php $count1++; } ?>
<?php endforeach; ?>
<!-- all the hit loop end-->
                                    </div>
                                </div>
                            </div>
                        </div>
  				    </div>
  			  </div>
              <div class="container-fluid updates">
                  <div class="container text-center">
      			      <h1 class="col-sm-12">Never miss interesting content update.<br>Get Justwords Blog email updates weekly in your inbox</h1>
      			      <button class="btn subscribe_btn">Subscribe to Email updates</button>
    			  </div>
  			  </div>
              <div class="container-fluid">
    			  <div class="container">
      			      <div class="row">
                          <div class="col-sm-4 blog_border" >
                          <!-- 4 loop -->
                          <?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=1&offset=3'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count5 = 0; if ($count5 == "1") { break; } else { ?>
<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

<img src="<?php echo $feat_image;  ?>"  />
<div class="blog_content">
<h4 class="h4top"><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>" style="color:#000000;text-decoration:none;">
<?php the_title(); ?></a></h4>


<p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span><a href="<?php echo get_comments_link( $post->ID ); ?>">
    Comments to this post
</a></span></p>
</div>
<?php $count1++; } ?>
<?php endforeach; ?>
<!-- 4 loop end-->
					      </div>
                          <div class="col-sm-4 blog_border" style="padding-left:7.5px;padding-right:7.5px">
                              <!-- 5 loop -->
                          <?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=1&offset=4'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count6 = 0; if ($count6 == "1") { break; } else { ?>
<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
<img src="<?php echo $feat_image;  ?>"  />
<div class="blog_content">
<h4><a href="<?php the_permalink() ?>" rel="bookmark"  title="Permanent Link to <?php the_title_attribute(); ?>" style="color:#000000;text-decoration:none;">
<?php the_title(); ?></a></h4>


<p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span><a href="<?php echo get_comments_link( $post->ID ); ?>">
    Comments to this post
</a></span></p>
</div>
<?php $count1++; } ?>
<?php endforeach; ?>
<!-- 5 loop end-->
					      </div>
                          <div class="col-sm-4 blog_border" >
                              <!-- 6 loop -->
                          <?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=1&offset=5'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count7 = 0; if ($count7 == "1") { break; } else { ?>
<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
<img src="<?php echo $feat_image;  ?>"  />
<div class="blog_content">
<h4><a href="<?php the_permalink() ?>" rel="bookmark"  title="Permanent Link to <?php the_title_attribute(); ?>" style="color:#000000;text-decoration:none;">
<?php the_title(); ?></a></h4>


<p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span><a href="<?php echo get_comments_link( $post->ID ); ?>">
    Comments to this post
</a></span></p>
</div>
<?php $count1++; } ?>
<?php endforeach; ?>
<!-- 6 loop end-->
					      </div>
                          <div class="col-sm-4 blog_border" >
                              <!-- 7 loop -->
                          <?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=1&offset=6'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count8 = 0; if ($count8 == "1") { break; } else { ?>
<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
<img src="<?php echo $feat_image;  ?>"  />
<div class="blog_content">
<h4><a href="<?php the_permalink() ?>" rel="bookmark"  title="Permanent Link to <?php the_title_attribute(); ?>" style="color:#000000;text-decoration:none;">
<?php the_title(); ?></a></h4>


<p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span><a href="<?php echo get_comments_link( $post->ID ); ?>">
    Comments to this post
</a></span></p>
</div>
<?php $count1++; } ?>
<?php endforeach; ?>
<!-- 7loop end-->
					      </div>
                          <div class="col-sm-4 blog_border" style="padding-left:7.5px;padding-right:7.5px">
                              <!-- 8loop -->
                          <?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=1&offset=7'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count9 = 0; if ($count9 == "1") { break; } else { ?>
<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
<img src="<?php echo $feat_image;  ?>"  />
<div class="blog_content">
<h4><a href="<?php the_permalink() ?>" rel="bookmark"  title="Permanent Link to <?php the_title_attribute(); ?>" style="color:#000000;text-decoration:none;">
<?php the_title(); ?></a></h4>


<p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span><a href="<?php echo get_comments_link( $post->ID ); ?>">
    Comments to this post
</a></span></p>
</div>
<?php $count1++; } ?>
<?php endforeach; ?>
<!-- 8loop end-->
					      </div>
                          <div class="col-sm-4 blog_border" >
                              <!-- 9loop -->
                          <?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=1&offset=8'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count10 = 0; if ($count10 == "1") { break; } else { ?>
<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
<img src="<?php echo $feat_image;  ?>"  />
<div class="blog_content">
<h4><a href="<?php the_permalink() ?>" rel="bookmark"  title="Permanent Link to <?php the_title_attribute(); ?>" style="color:#000000;text-decoration:none;">
<?php the_title(); ?></a></h4>


<p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span><a href="<?php echo get_comments_link( $post->ID ); ?>">
    Comments to this post
</a></span></p>
</div>
<?php $count1++; } ?>
<?php endforeach; ?>
<!-- 9loop end-->
					      </div>
                          <!-- 15loop -->
                          <?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=1&offset=19'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count17= 0; if ($count17 == "1") { break; } else { ?>

<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>


					<div class="col-sm-4 blog_border"> <img src="<?php echo $feat_image;  ?>"  />
                              <div class="blog_content">
						           <h4><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>">
<?php the_title(); ?></a></h4>
                                 <p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span> <a href="<?php echo get_comments_link( $post->ID ); ?>">
    Comments to this post
</a></span></p>
					          </div>
					      </div>	 
						   
				
  				  <?php $count1++; } ?>
<?php endforeach; ?>
<!-- 15loop end-->

                          -------------------
                          <div class="col-sm-4 blog_border">
                              <img src="<?php echo bloginfo('url'); ?>/vertiver/images/JWC%20Blog%20Image17-6(6).jpg">
                              <div class="blog_content">
						          <h4><a href="#" title="">13 actionable copywriting tricks to get your SEO content rocking</a></h4>
                                  <p><span>Jun 17, 2016</span><span> | </span><span>Payel Mukherjee</span><span> | </span><span>0 Comments</span></p>
					          </div>
					      </div>
                          <div class="col-sm-4 blog_border">
                              <div class="get_book getbooknew" style="padding-bottom:19px;">
                                  <h3 class="ebookh3">Get your free<br>E-book</h3>
                                  <p class="ebookp" style="">Download our free case study to know how you can increase your business via content marketing. Download our free case study content marketing.</p>
                                  <a class="ebooka btn red-btn"  href="#"  style="margin-top:20px;background:#E88A0E;color:#000000;">Download My Copy</a>
                              </div>
					      </div>
                      </div>
  			      </div>
                  <div class="container seo_desc">
      			      <div class="row">
                          <div class="col-sm-8">
          		              <a class="titlehreftop" href="#">
                              <img src="<?php echo bloginfo('url'); ?>/vertiver/images/fail%20proof.jpg"></a>
        		          </div>
                          <div class="col-sm-4 blog_master">
          		              <div class="blogs">
            		              <h4><a style="font-size:30px;" class="titlehreftop" href="#">A Fail-Proof 10-Step Guide to Good Content</a></h4>
            		              <p><span>Jan 5, 2016</span><span> | </span><span>Payel Mukherjee</span><span> | </span><span>1 Comments</span></p>
                                  <p style="font-size:21px;">You get it! Content is extreme</p>
                              </div>
                          </div>
                      </div>
    		      </div>
                  <div class="container">
      			      <div class="row">

                           <!-- 12loop -->
                          <?php query_posts('showposts=2'); ?>
<?php $posts = get_posts('numberposts=2&offset=14'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count13= 0; if ($count13 == "2") { break; } else { ?>

<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>


					<div class="col-sm-4 blog_border"> <img src="<?php echo $feat_image;  ?>"  />
                              <div class="blog_content">
						           <h4><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>">
<?php the_title(); ?></a></h4>
                                 <p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span> <a href="<?php echo get_comments_link( $post->ID ); ?>">
    Comments to this post
</a></span></p>
					          </div>
					      </div>	 
						   
				
  				  <?php $count1++; } ?>
<?php endforeach; ?>
<!-- 12loop end-->
                          <div class="col-sm-4 blog_border">  
						      <a href="#" title="#"><img src="<?php echo bloginfo('url'); ?>/vertiver/images/dodont.jpg" style="width:auto;height: 379px;"></a>
                          </div>

                           <!-- 10loop -->
                          <?php query_posts('showposts=3'); ?>
<?php $posts = get_posts('numberposts=3&offset=9'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count11 = 0; if ($count11 == "3") { break; } else { ?>

<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>


					<div class="col-sm-4 blog_border"> <img src="<?php echo $feat_image;  ?>"  />
                              <div class="blog_content">
						           <h4><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>">
<?php the_title(); ?></a></h4>
                                 <p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span> <a href="<?php echo get_comments_link( $post->ID ); ?>">
    Comments to this post
</a></span></p>
					          </div>
					      </div>	 
						   
				
  				  <?php $count1++; } ?>
<?php endforeach; ?>
<!-- 10loop end-->
		

                      </div>
      			  </div>
                  <div class="container seo_desc">
                      <div class="row">
                          <div class="col-sm-8">
							<a class="titlehreftop" href="#"><img src="<?php echo bloginfo('url'); ?>/vertiver/images/JWC%20Blog%20Image26-10_0.jpg"></a>
						  </div>
                          <div class="col-sm-4 blog_master">
							  <div class="blogs">
							      <h4><a style="font-size:30px;" class="titlehreftop" href="#">Want rocking social media posts? Try these 10 ideas</a></h4>
							      <p><span>Oct 26, 2016</span><span> | </span><span>Amaresh Choudhary</span><span> | </span><span>0 Comments</span></p>
							      <p style="font-size:21px;">One of the discussions I often have with my social media content writeris how to create social content t</p>
							  </div>
						  </div>
                      </div>
				  </div>
                  <div class="container">
      			      <div class="row">


                          <!-- 11loop -->
                          <?php query_posts('showposts=2'); ?>
<?php $posts = get_posts('numberposts=2&offset=12'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count12 = 0; if ($count12 == "2") { break; } else { ?>

<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>


					<div class="col-sm-4 blog_border"> <img src="<?php echo $feat_image;  ?>"  />
                              <div class="blog_content">
						           <h4><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>">
<?php the_title(); ?></a></h4>
                                 <p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span> <a href="<?php echo get_comments_link( $post->ID ); ?>">
    Comments to this post
</a></span></p>
					          </div>
					      </div>	 
						   
				
  				  <?php $count1++; } ?>
<?php endforeach; ?>
<!-- 11loop end-->
                          <div class="col-sm-4 blog_border">  
						      <a href="#" title="#"><img src="<?php echo bloginfo('url'); ?>/vertiver/images/dodont.jpg" style="width:auto;height: 379px;"></a>
                          </div>
                          <div class="col-sm-4 blog_border">
                              <img src="<?php echo bloginfo('url'); ?>/vertiver/images/JWC%20Blog%20Image11-01-2017a.jpg">
                              <div class="blog_content">
						          <h4><a href="#" title="#">Adding some creativity to Google Business Photos</a></h4>
                                  <p><span>Apr 8, 2016</span><span> | </span><span>Payel Mukherjee</span><span> | </span><span>0 Comments</span></p>
					          </div>
					      </div>
                          <div class="col-sm-4 blog_border">
                              <img src="<?php echo bloginfo('url'); ?>/vertiver/images/JWC%20Blog%20Image11-01-2017.jpg">
                              <div class="blog_content">
						          <h4><a href="#" title="#">Are you building a brand? Then tell a story.</a></h4>
                                  <p><span>Apr 8, 2016</span><span> | </span><span>Amaresh Choudhary</span><span> | </span><span>0 Comments</span></p>
					          </div>
					      </div>
                          <!-- 13loop -->
                          <?php query_posts('showposts=1'); ?>
<?php $posts = get_posts('numberposts=2&offset=16'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count14= 0; if ($count14 == "1") { break; } else { ?>

<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>


					<div class="col-sm-4 blog_border"> <img src="<?php echo $feat_image;  ?>"  />
                              <div class="blog_content">
						           <h4><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>">
<?php the_title(); ?></a></h4>
                                 <p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span> <a href="<?php echo get_comments_link( $post->ID ); ?>">
    Comments to this post
</a></span></p>
					          </div>
					      </div>	 
						   
				
  				  <?php $count1++; } ?>
<?php endforeach; ?>
<!-- 13loop end-->
                      </div>
      			  </div>
                  <div class="container seo_desc">
				      <div class="row">
                          <div class="col-sm-8">
							  <a class="titlehreftop" href="#"><img src="<?php echo bloginfo('url'); ?>/vertiver/images/Hiring%20content%20agency.jpg"></a>
						  </div>
                          <div class="col-sm-4 blog_master">
							  <div class="blogs">
							      <h4><a style="font-size:30px;" class="titlehreftop" href="#">11 things you should know before hiring a content agency</a></h4>
							      <p><span>Nov 22, 2016</span><span> | </span><span>Payel Mukherjee</span><span> | </span><span>0 Comments</span></p>
							      <p style="font-size:21px;">Well, if you have not been living under a rock, you know by now that content marketing is the new thin</p>
							  </div>
						  </div>
                      </div>
				  </div>
                  <div class="container">
      			      <div class="row">
                          <!-- 14loop -->
                          <?php query_posts('showposts=2'); ?>
<?php $posts = get_posts('numberposts=2&offset=17'); foreach ($posts as $post) : start_wp(); ?>
<?php static $count16= 0; if ($count16 == "2") { break; } else { ?>

<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>


					<div class="col-sm-4 blog_border"> <img src="<?php echo $feat_image;  ?>"  />
                              <div class="blog_content">
						           <h4><a href="<?php the_permalink() ?>" rel="bookmark" class="titlehreftop" title="Permanent Link to <?php the_title_attribute(); ?>">
<?php the_title(); ?></a></h4>
                                 <p><span><?php the_time('F jS, Y') ?></span><span> | </span><span><?php  the_author();  ?></span><span> | </span><span> <a href="<?php echo get_comments_link( $post->ID ); ?>">
    Comments to this post
</a></span></p>
					          </div>
					      </div>	 
						   
				
  				  <?php $count1++; } ?>
<?php endforeach; ?>
<!-- 14loop end-->
                          <div class="col-sm-4 blog_border"><a href="#" title="3"><img src="<?php echo bloginfo('url'); ?>/vertiver/images/dodont.jpg" style="width:auto;height: 379px;" /></a></div>
                              <div class="col-sm-4 blog_border"><img src="<?php echo bloginfo('url'); ?>/vertiver/images/fail%20proof.jpg" />
                                  <div class="blog_content">
						              <h4><a href="#" title="#">A Fail-Proof 10-Step Guide to Good Content</a></h4>
                                      <p><span>Jan 5, 2016</span><span> | </span><span>Payel Mukherjee</span><span> | </span><span>1 Comments</span></p>
					              </div>
					          </div>
                              <div class="col-sm-4 blog_border">
                                  <img src="<?php echo bloginfo('url'); ?>/vertiver/images/Tips%20To%20Increase%20Social%20Engagement%20On%20Your%20Blog.jpg">
                                  <div class="blog_content">
						              <h4><a href="#" title="3">Tips to increase social engagement on your blog</a></h4>
                                      <p><span>Jan 5, 2016</span><span> | </span><span>Payel Mukherjee</span><span> | </span><span>2 Comments</span></p>
					              </div>
					          </div>
                              <div class="col-sm-4 blog_border"><img src="<?php echo bloginfo('url'); ?>/vertiver/images/freshness.jpg">
                                  <div class="blog_content">
						              <h4><a href="#" title="#">The freshness factor for a successful website</a></h4>
                                      <p><span>Jan 5, 2016</span><span> | </span><span>Payel Mukherjee</span><span> | </span><span>0 Comments</span></p>
					              </div>
					          </div>
                      </div>
      			  </div>
                  <div class="container seo_desc">
					<div class="row">
                        <div class="col-sm-8"><a class="titlehreftop" href="#"><img src="<?php echo bloginfo('url'); ?>/vertiver/images/Girl%20Working.jpg"></a></div>
                        <div class="col-sm-4 blog_master">
							<div class="blogs">
							    <h4><a style="font-size:30px;" class="titlehreftop" href="#">Find content proofreading boring: Here are 7 brilliant ways to get the job done</a></h4>
							    <p><span>Feb 4, 2017</span><span> | </span><span>Rajrupa Ghosh</span><span> | </span><span>0 Comments</span></p>
							    <p style="font-size:21px;">Sitting down to write is one thing. Sending it across for consumption is another. And making sure that your copy is clean and error-free – that is </p>
							</div>
						</div>
                    </div>
				</div>
                <div class="container">
    			    <div class="row">
      			        <div class="daily_updates">
        			        <h1 class="col-sm-12">Get Justwords Daily</h1>
        			        <div class="col-sm-12">
                                <form action="#" method="post" id="simple-subscription-form--2" accept-charset="UTF-8">
                                    <div>
                                        <div class="form-type-textfield form-item-mail form-item form-group">
                                            <input class="form-control form-text required" type="text" id="edit-mail--2" name="mail" value="" size="20" maxlength="255" />
                                        </div>
                                        <button class="btn btn-default form-submit" id="edit-submit--2" name="op" value="Subscribe" type="submit">Subscribe</button>
                                        <input type="hidden" name="form_build_id" value="form-14LX8fhxGH8pw4nn9Mz02CJjAmDEO5u3VSaZid5vNgA" />
                                        <input type="hidden" name="form_id" value="simple_subscription_form" />
                                    </div>
                                </form>
                            </div>
      			        </div>
    			    </div>
  			    </div><!-- Modal -->
                <div id="myBlogModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content subscribe_body">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <h2>If you are enjoying what you are<br/> reading, why not subscribe to our blog!</h2>
                                <p>We promise to deliver you one hell of an intersting emailer every week telling you about what is happening in the content marketing industry, about content strategies of different companies, and content best practices from the around world!</p>
                                <p>This one is not your average newsletter!</p>
                                <p><form action="#" method="post" id="simple-subscription-form--3" accept-charset="UTF-8">
                                    <div>
                                        <div class="form-type-textfield form-item-mail form-item form-group">
                                            <input class="form-control form-text required" type="text" id="edit-mail--3" name="mail" value="" size="20" maxlength="255" />
                                        </div>
                                        <button class="btn btn-default form-submit" id="edit-submit--3" name="op" value="Subscribe" type="submit">Subscribe</button>
                                        <input type="hidden" name="form_build_id" value="form-Tgbe0A07BvIadozoyCcIgzqGE3DDMWjKw_Xdedarbvg" />
                                        <input type="hidden" name="form_id" value="simple_subscription_form" />
                                    </div>
                                </form></p>
                            </div>
                            <!--div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div-->
                        </div>
                    </div>
                </div>
            </section> <!-- /.block -->
        </div>
    </section>
    <!-- Subscribe Modal -->
    <div id="subMsg" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content subscribe_body">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        &times;</button>
                </div>
                <div class="modal-body">
                    <h2 class="modal-title">
                        Thanks, you are signed<br>
                        up for our blog updates!
                    </h2>
                    <p>
                        We would love your help in spreading the word about the Justwords blog!
                    </p>
                    <button class="btn btn-subsc">
                        Click here to go to our latest article.</button>
                </div>
                <!--div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div-->
            </div>
        </div>
    </div>
    <!-- subscribe modal ends here -->
    
    

<?php get_footer();
