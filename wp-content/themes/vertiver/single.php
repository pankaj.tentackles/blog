<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="container-fluid blogs_inner">
<div class="container">
                    <div class="row">
                        <div class="col-sm-8">
                        <div class="blogs_left">

			<?php
				/* Start the Loop */
				while ( have_posts() ) : the_post();  ?>   


                       
<?php

get_template_part( 'template-parts/post/content', get_post_format() );

 ?> 
<p>

<script charset="utf-8" type="text/javascript">var switchTo5x=true;</script>
<script charset="utf-8" type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script charset="utf-8" type="text/javascript">stLight.options({"publisher":"wp.9b4a3a96-9fda-4e61-9395-411bc31e5802"});var st_type="wordpress3.7.3";</script>


<span class='st_facebook' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
<span class='st_twitter' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
<span class='st_linkedin' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
<span class='st_email' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
<span class='st_sharethis' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
<span class='st_fblike' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
<span class='st_plusone' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
<span class='st_pinterest' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
</p>


<?php

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

					

				endwhile; // End of the loop.
			?>

	</div></div>
    <div class="col-sm-4 right_blog">
	<?php get_sidebar(); ?>
    
</div></div> </div></div><!-- .wrap -->

<?php get_footer();
