<ul class="et-social-icons">

<?php if ( 'on' === et_get_option( 'divi_show_facebook_icon', 'on' ) ) : ?>
	<li class="et-social-icon et-social-facebook">
		<a href="<?php echo esc_url( et_get_option( 'divi_facebook_url', 'https://www.facebook.com/vertiver' ) ); ?>" class="icon">
			<span><?php esc_html_e( 'Facebook', 'Divi' ); ?></span>
		</a>
	</li>
<?php endif; ?>
<?php if ( 'on' === et_get_option( 'divi_show_twitter_icon', 'on' ) ) : ?>
	<li class="et-social-icon et-social-twitter">
		<a href="<?php echo esc_url( et_get_option( 'divi_twitter_url', 'https://twitter.com/search?q=vertiver&src=sprv' ) ); ?>" class="icon">
			<span><?php esc_html_e( 'Twitter', 'Divi' ); ?></span>
		</a>
	</li>
<?php endif; ?>
<?php if ( 'on' === et_get_option( 'divi_show_pinterest_icon', 'on' ) ) : ?>
	<li class="et-social-icon et-social-pinterest">
		<a href="<?php echo esc_url( et_get_option( 'divi_pinterest_url', 'https://www.pinterest.com/vertiver/' ) ); ?>" class="icon">
			<span><?php esc_html_e( 'Pinterest', 'Divi' ); ?></span>
		</a>
	</li>
<?php endif; ?>
<?php if ( 'on' === et_get_option( 'divi_show_youtube_icon', 'on' ) ) : ?>
<?php
	$et_rss_url = '' !== et_get_option( 'divi_youtube_url' )
		? et_get_option( 'divi_youtube_url' )
		: get_bloginfo( 'rss2_url' );
?>
	<li class="et-social-icon et-social-youtube">
		<a href="<?php echo esc_url( et_get_option( 'divi_youtube_url', 'https://www.youtube.com/user/AgencyVertiver' ) ); ?>" class="icon">
			<span><?php esc_html_e( 'Youtube', 'Divi' ); ?></span>
		</a>
	</li>
<?php endif; ?>


<?php if ( 'on' === et_get_option( 'divi_show_vimeo_icon', 'on' ) ) : ?>
<?php
	$et_rss_url = '' !== et_get_option( 'divi_vimeo_url' )
		? et_get_option( 'divi_vimeo_url' )
		: get_bloginfo( 'rss2_url' );
?>
	<li class="et-social-icon et-social-vimeo">
		<a href="<?php echo esc_url( et_get_option( 'divi_vimeo_url', 'https://vimeo.com/vertiver' ) ); ?>" class="icon">
			<span><?php esc_html_e( 'Vimeo', 'Divi' ); ?></span>
		</a>
	</li>
<?php endif; ?>


<?php if ( 'on' === et_get_option( 'divi_show_linkedin_icon', 'on' ) ) : ?>
<?php
	$et_rss_url = '' !== et_get_option( 'divi_linkedin_url' )
		? et_get_option( 'divi_linkedin_url' )
		: get_bloginfo( 'rss2_url' );
?>
	<li class="et-social-icon et-social-linkedin ">
		<a href="<?php echo esc_url( et_get_option( 'divi_linkedin_url', 'https://www.linkedin.com/company/vertiver/' ) ); ?>" class="icon">
			<span><?php esc_html_e( 'Linkedin', 'Divi' ); ?></span>
		</a>
	</li>
<?php endif; ?>

</ul>