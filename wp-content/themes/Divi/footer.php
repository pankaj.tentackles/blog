<?php if ( 'on' == et_get_option( 'divi_back_to_top', 'false' ) ) : ?>



<span class="et_pb_scroll_top et-pb-icon"></span>



<?php endif;



if ( ! is_page_template( 'page-template-blank.php' ) ) : ?>



			<footer id="main-footer">

				<?php get_sidebar( 'footer' ); ?>





		<?php

			if ( has_nav_menu( 'footer-menu' ) ) : ?>



				<div id="et-footer-nav">

					<div class="container">

						<?php

							wp_nav_menu( array(

								'theme_location' => 'footer-menu',

								'depth'          => '1',

								'menu_class'     => 'bottom-nav',

								'container'      => '',

								'fallback_cb'    => '',

							) );

						?>

					</div>

				</div> <!-- #et-footer-nav -->



			<?php endif; ?>



				<div id="footer-bottom">

					<div class="container clearfix">

				<?php

					if ( false !== et_get_option( 'show_footer_social_icons', true ) ) {

						get_template_part( 'includes/social_icons', 'footer' );

					}



					echo et_get_footer_credits();

				?>

					</div>	<!-- .container -->

				</div>

			</footer> <!-- #main-footer -->

		</div> <!-- #et-main-area -->



<?php endif; // ! is_page_template( 'page-template-blank.php' ) ?>



	</div> <!-- #page-container -->







	<?php wp_footer(); ?>

</body>



<script src='https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js'></script>
<script src='https://threejs.org/examples/js/libs/stats.min.js'></script>
<script src="<?php echo esc_url( $template_directory_uri . '/js/index.js"' ); ?>" type="text/javascript"></script>

<script>

particlesJS("particles-js", {
    "particles": {
        "number": {
            "value": 160,
            "density": {
                "enable": true,
                "value_area": 800
            }
        },
        "color": {
            "value": "#ffffff"
        },
        "shape": {
            "type": "circle",
            "stroke": {
                "width": 0,
                "color": "#000000"
            },
            "polygon": {
                "nb_sides": 5
            },
            "image": {
                "src": "img/github.svg",
                "width": 100,
                "height": 100
            }
        },
        "opacity": {
            "value": 0.3928359549120531,
            "random": true,
            "anim": {
                "enable": true,
                "speed": 1,
                "opacity_min": 0,
                "sync": false
            }
        },
        "size": {
            "value": 4,
            "random": true,
            "anim": {
                "enable": false,
                "speed": 4,
                "size_min": 0.3,
                "sync": false
            }
        },
        "line_linked": {
            "enable": false,
            "distance": 150,
            "color": "#ffffff",
            "opacity": 0.1,
            "width": 1
        },
        "move": {
            "enable": true,
            "speed": 0,
            "direction": "none",
            "random": true,
            "straight": false,
            "out_mode": "out",
            "bounce": false,
            "attract": {
                "enable": true,
                "rotateX": 2367.442924896818,
                "rotateY": 1420.4657549380909
            }
        }
    },
    "interactivity": {
        "detect_on": "canvas",
        "events": {
            "onhover": {
                "enable": false,
                "mode": "bubble"
            },
            "onclick": {
                "enable": false,
                "mode": "repulse"
            },
            "resize": true
        },
        "modes": {
            "grab": {
                "distance": 255.80432187492372,
                "line_linked": {
                    "opacity": 1
                }
            },
            "bubble": {
                "distance": 250,
                "size": 0,
                "duration": 2,
                "opacity": 0,
                "speed": 3
            },
            "repulse": {
                "distance": 400,
                "duration": 0.4
            },
            "push": {
                "particles_nb": 4
            },
            "remove": {
                "particles_nb": 2
            }
        }
    },
    "retina_detect": true
});


</script>

</html>





<script>

jQuery(document).ready(function($)){

     $('.newsletter-joinus .es_textbox_class').attr('placeholder', 'Enter email address');

     $('.newsletter-joinus .es_submit_button').val('Join Us!');



    $('.newsletter-signup .es_textbox_class').attr('placeholder', 'Enter email address');

    $('.newsletter-signup .es_submit_button').val('SIGN UP');

});



</script>